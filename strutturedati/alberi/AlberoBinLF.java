package strutturedati.alberi;

import strutturedati.alberi.util.*;
import java.util.Iterator;

public class AlberoBinLF<T> extends AlberoLF<T> implements AlberoBin<T> {
	protected final int FIGLIO_SINISTRO = 0;
	protected final int FIGLIO_DESTRO = 1;
	
	public AlberoBinLF() {
		super(2);
	}
	
	public AlberoBinLF(T val) {
		super(2, val);
	}
	
	public AlberoBinLF<T> figlioSinistro() {
		return (AlberoBinLF<T>)figli.get(FIGLIO_SINISTRO);
	}
	
	public AlberoBinLF<T> figlioDestro() {
		return (AlberoBinLF<T>)figli.get(FIGLIO_DESTRO);
	}
	
	public boolean setFiglioSinistro(AlberoBinLF<T> nodo) {
		return setFiglio(nodo, FIGLIO_SINISTRO);
	}
	
	public boolean setFiglioDestro(AlberoBinLF<T> nodo) {
		return setFiglio(nodo, FIGLIO_DESTRO);
	}
	
	public AlberoBinLF<T> padre() {
		return (AlberoBinLF<T>)padre;
	}
	
	public Iterator<T> iterator() {
		return new IteratorVI(this);
	}
	
	protected class IteratorVI implements Iterator<T>{
		
		protected static final int SIN = 0;
		protected static final int DES = 1;
		protected static final int SU = 2;
		protected static final int STOP = 3;
		
		protected AlberoBin<T> curr;
		protected boolean hasNext;
		
		public IteratorVI(AlberoBin<T> a){
			curr=a;
			hasNext=true;
		}


		private void succ() {
			int dir = DES;
			while(dir!=STOP){
				switch (dir){
				case DES:
					if(curr.figlioDestro()==null){
						dir=SU;
					}else{
						curr=curr.figlioDestro();
						dir=SIN;
					}
					break;
				case SIN:
					if(curr.figlioSinistro()==null){
						hasNext=true;
						dir=STOP;
					}else{
						curr= curr.figlioSinistro();
						dir=SIN;
					}
					break;
				case SU:
					if(curr.padre()==null){
						hasNext=false;
						dir=STOP;
					}else{
						if(((AlberoBin<T>) curr.padre()).figlioSinistro() ==curr){
							curr=(AlberoBin<T>) curr.padre();
							hasNext=true;
							dir=STOP;
						}else{
							curr=(AlberoBin<T>) curr.padre();
							dir = SU;
						}
					}
					break;
				}
			}
			
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return hasNext;
		}

		@Override
		public T next() {
			if(!hasNext()) return null;
			T tmp = curr.val();
			succ();
			return tmp;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	public static void main(String[] args) {
		System.exit(0);
	}
}
