package strutturedati.alberi.util;

/*
	Classe vettore per contenere i figli di un nodo
*/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Vettore<T> {
	private Object[] items; // vettore in cui andiamo ad inserire i nodi figlio
	private int size; // la size corrisponde al grado del nodo
	
	public Vettore(int sizeMax) {
		items = new Object[sizeMax];
	}
	
	public void set(int pos, T e) {
		if(items[pos] != null) size++;
		items[pos] = e;
	} 
	
	public void remove(int pos) {
		if(items[pos] != null) {
			items[pos] = null;
			size--;
		}
	}
	
	@SuppressWarnings("unchecked")
	public T get(int pos) {
		return (T) items[pos];
	}
	
	public int size() {
		return size;
	}
	
	public Iterator<T> iterator() {
		return new IteratorFigli();
	}
	
	/*
		Iteratore sui figli, restituisce i figli non nulli
	*/
	
	private class IteratorFigli implements Iterator<T> {
		private int pos = -1;
		private boolean hasNext;
		
		public IteratorFigli() {
			succ();
		}
		
		public boolean hasNext() {
			return hasNext;
		}
		
		@SuppressWarnings("unchecked")
		public T next() {
			if(!hasNext()) throw new NoSuchElementException();
            Object tmp = items[pos];
            succ();
            return (T) tmp;
		}
		
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
		/*
			Aggiorna pos al prossimo figlio non null
		*/
		private void succ() {
			hasNext = false;
			pos++;
			while(pos < items.length) {
				if(items[pos] != null) {
					hasNext = true;
					return;
				}
				pos++;
			}
		}
	}//
}
