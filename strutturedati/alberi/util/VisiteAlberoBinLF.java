package strutturedati.alberi.util;

/**
	Classe di supporto per le visite agli AlberiBinLF<T>
*/

import strutturedati.alberi.*;
import java.util.*;

public class VisiteAlberoBinLF<T> extends VisiteAlberoLF<T> {
	public VisiteAlberoBinLF(Albero<T> nodo) {
		super(nodo);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> visitaInfissa() {
		List<T> lista = new LinkedList<T>();
		visitaInfissa((AlberoBinLF<T>)nodo, lista);
		return lista;
	}
	
	private void visitaInfissa(AlberoBinLF<T> nodo, List<T> lista) {
		if(nodo.figlioSinistro() != null) 
			visitaInfissa(nodo.figlioSinistro(), lista);
		lista.add(nodo.val());
		if(nodo.figlioDestro() != null)
			visitaInfissa(nodo.figlioDestro(), lista);
	}
}
