package strutturedati.alberi.util;

/**
	Classe di supporto per le visite agli AlberiLF<T>
*/

import strutturedati.alberi.*;
import java.util.*;

public class VisiteAlberoLF<T> {
	protected Albero<T> nodo;
	
	public VisiteAlberoLF(Albero<T> nodo) {
		this.nodo = nodo;
	}
	/**
		Visita inOrder di un albero
		@param nodo il nodo da cui si vuole partire
		@return lista con gli elementi dell'albero inOrder
	*/
	public List<T> visitaAnticipata() {
		List<T> lista = new LinkedList<T>();
		visitaAnticipata(nodo, lista);
		return lista;
	}
	
	private void visitaAnticipata(Albero<T> nodo, List<T> l) {
		l.add(nodo.val());
		Iterator<Albero<T>> it = nodo.iteratorFigli();
		while(it.hasNext())
			visitaAnticipata(it.next(), l);
	}
	
	/**
		Visita postOrder
		@param nodo il nodo da cui si vuole partire
		@return lista con gli elementi dell'albero postOrder
	*/
	public List<T> visitaPosticipata() {
		List<T> lista = new LinkedList<T>();
		visitaPosticipata(nodo, lista);
		return lista;
	}
	
	private void visitaPosticipata(Albero<T> nodo, List<T> l) {
		Iterator<Albero<T>> it = nodo.iteratorFigli();
		while(it.hasNext())
			visitaAnticipata(it.next(), l);
		l.add(nodo.val());
	}
	
	/**
		Visita per livelli
	*/
	public List<T> visitaLivelli() {
		List<T> lista = new LinkedList<T>();
		Queue<Albero<T>> daVisitare = new LinkedList<Albero<T>>();
		daVisitare.offer(nodo);
		while(!daVisitare.isEmpty()) {
			Albero<T> visitato = daVisitare.poll();
			lista.add(visitato.val());
			Iterator<Albero<T>> it = visitato.iteratorFigli();
			while(it.hasNext())
				daVisitare.offer(it.next());
		}
		return lista;
	}

}
