package strutturedati.alberi;

/*
	Implementazione di un albero d-ario 
	con la rappresentazione "Lista Padri"
*/

import strutturedati.alberi.util.*;
import java.util.Iterator;

@SuppressWarnings("rawtypes")
public class AlberoLF<T> implements Albero<T> {
	protected T val;
	protected AlberoLF<T> padre;
	protected Vettore<AlberoLF<T>> figli;
	protected int posFiglio = -1; // se this è figlio di un nodo, indica la posizione che ha un vettore figli
	protected int gradoMax;
	
	public AlberoLF(int gradoMax) {
		this.gradoMax = gradoMax;
		figli = new Vettore<AlberoLF<T>>(gradoMax);
	}
	
	public AlberoLF(int gradoMax, T val) {
		this(gradoMax);
		this.val = val;
	}
	
	public T val() {
		return val;
	}
	
	public void setVal(T val) {
		this.val = val;
	}
	
	public Albero<T> padre() {
		return padre;
	}
	
	public boolean setFiglio(AlberoLF<T> figlio, int pos) {
		if(figlio == null) 
			return true; // gli alberi vuti sono già presenti nel vettore figli
		if(figlio.gradoMax() != this.gradoMax) 
			return false; //gli alberi sono diversi
		if(figli.get(pos) != null) return false;
		figli.set(pos, figlio);
		figlio.padre = this; // il padre di figlio è proprio this
		figlio.posFiglio = pos;
		return true;
	}
	
	public Albero<T> getFiglio(int pos) {
		return figli.get(pos);
	}
	
	public int getPos() {
		return posFiglio;
	}
	
	public int grado() {
		return figli.size();
	}
	
	public int gradoMax() {
		return gradoMax;
	}
	
	/*
		Stacca il corrente AlberoLF da suo padre
	*/
	public void pota() {
		if(padre != null) { // non deve essere la radice
			padre.figli.remove(posFiglio);
			padre = null;
			posFiglio = -1;
		}
	}
	
	public Iterator<Albero<T>> iteratorFigli() {
		return new Iteratore(figli.iterator());
	}
	
	protected class Iteratore implements Iterator<Albero<T>> {
		protected Iterator<AlberoLF<T>> it; 
		
		public Iteratore(Iterator<AlberoLF<T>> it) {
			this.it = it;
		}
		
		@Override
		public boolean hasNext() {
			return it.hasNext();
		}
		
		@Override
		public Albero<T> next() {
			AlberoLF<T> tmp = it.next();
			return tmp;
		}
		
		@Override
		public void remove() {
			it.remove();
		}
	}
	
	public Iterator<T> iterator() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		AlberoLF<Integer> albero = new AlberoLF<Integer>(4, 3);
		albero.setFiglio(new AlberoLF<Integer>(4, 2), 1);
		albero.setFiglio(new AlberoLF<Integer>(4, 32), 3);
		AlberoLF<Integer> albero2 = new AlberoLF<Integer>(4, 89);
		albero2.setFiglio(new AlberoLF<Integer>(4, 34), 1);
		albero2.setFiglio(new AlberoLF<Integer>(4, 36), 3);
		albero.setFiglio(albero2, 2);
		
		System.out.println(new VisiteAlberoLF(albero).visitaAnticipata().toString());
		System.out.println(new VisiteAlberoLF(albero).visitaPosticipata().toString());
		System.out.println(new VisiteAlberoLF(albero).visitaLivelli().toString());
		
	}
}
