package strutturedati.alberi;

/**
	Implementazione di un albero binario
	di ricerca
*/

import strutturedati.alberi.util.*;
import java.util.*;

@SuppressWarnings("rawtypes")
public class ABR<T extends Comparable<? super T>> implements Dizionario<T> {
	protected AlberoBinLF<T> radice;
	
	@SuppressWarnings("unchecked")
	public void inserisci(T x) {
		AlberoBinLF<T> tmp = cercaNodo(x);
		if(tmp == null) { //inserisco la radice
			radice = new AlberoBinLF(x);
		} else if(!tmp.val().equals(x)) {
			AlberoBinLF<T> figlio = new AlberoBinLF(x);
			if(tmp.val().compareTo(x) < 0)
				tmp.setFiglioDestro(figlio);
			else
				tmp.setFiglioSinistro(figlio);
		}
	}
	
	public void rimuovi(T x) {
		AlberoBinLF<T> tmp = cercaNodo(x);
		if(tmp == null || !tmp.val().equals(x)) return;
		if(tmp.figlioSinistro() == null && tmp.figlioDestro() == null) { //è una foglia
			rimuoviNodo(tmp);
		} else {
			AlberoBinLF<T> tmp2 = massimo(tmp.figlioSinistro());
			tmp.setVal(tmp2.val());
			rimuoviNodo(tmp2);
		}
	}
	
	protected void rimuoviNodo(AlberoBinLF<T> nodo) {
		AlberoBinLF<T> figlio = (nodo.figlioSinistro() != null) ? nodo.figlioSinistro() : nodo.figlioDestro();
		AlberoBinLF<T> padre = nodo.padre();
		int pos = nodo.getPos();
		nodo.pota();
		if(figlio != null) 
			figlio.pota();
		if (padre != null) padre.setFiglio(figlio, pos);
		else radice = figlio;
	}
	
	/**
		Restituisce il valore cercato se è presente
		null altrimenti
	*/
	public T cerca(T x) {
		AlberoBinLF<T> tmp = cercaNodo(x);
		if(tmp != null && tmp.val().equals(x))
			return tmp.val();
		return null;
	}
	
	/**
		@param x nodo da cercare
		@return il nodo da cui deve venire l'inserimento di x
	*/
	
	protected AlberoBinLF<T> cercaNodo(T x) {
		if(radice == null) return null;
		AlberoBinLF<T> curr = radice;
		while(!curr.val().equals(x)) {
			if(curr.val().compareTo(x) < 0) {
				if(curr.figlioDestro() == null)
					return curr;
				curr = curr.figlioDestro();
			} else {
				if(curr.figlioSinistro() == null)
					return curr;
				curr = curr.figlioSinistro();
			}
		}
		return curr;
	}
	
	public T massimo() {
		return massimo(radice).val();
	}
	
	protected AlberoBinLF<T> massimo(AlberoBinLF<T> nodo) {
		AlberoBinLF<T> curr = nodo;
		while(curr.figlioDestro() != null) {
			curr = curr.figlioDestro();
		}
		return curr;
	}
	
	protected AlberoBinLF<T> predecessore(AlberoBinLF<T> nodo) {
		if(nodo.figlioSinistro() != null)
			return massimo(nodo.figlioSinistro());
		while(nodo.padre() != null && nodo.padre().figlioSinistro() == nodo) 
			nodo = nodo.padre();
		return nodo.padre();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> visitaInfissa() {
		return new VisiteAlberoBinLF(radice).visitaInfissa();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> visitaAnticipata() {
		return new VisiteAlberoBinLF(radice).visitaAnticipata();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> visitaPosticipata() {
		return new VisiteAlberoBinLF(radice).visitaPosticipata();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> visitaLivelli() {
		return new VisiteAlberoBinLF(radice).visitaLivelli();
	}
	
	public static void main(String[] args) {
		ABR<Integer> abr = new ABR<Integer>();
		abr.inserisci(5);
		abr.inserisci(-2);
		abr.inserisci(30);
		abr.inserisci(23);
		System.out.println("Visita infix\t" + abr.visitaInfissa().toString());
		System.out.println("Visita prefix\t" + abr.visitaAnticipata().toString());
		System.out.println("Visita postfix\t" + abr.visitaPosticipata().toString());
		System.out.println("Visita Livelli\t" + abr.visitaLivelli().toString());
		System.out.println("Massimo Albero\t" + abr.massimo());
		System.out.println("Predecessore di " + abr.cercaNodo(23).val() + " = " + abr.predecessore(abr.cercaNodo(23)).val());
		System.out.println("Rimuovo\t\t" + 23);
		abr.rimuovi(23);
		System.out.println("Visita infix\t" + abr.visitaInfissa().toString());
		System.out.println("Predecessore di " + abr.cercaNodo(23).val() + " = " + abr.predecessore(abr.cercaNodo(23)).val());
		
	}
}
