package strutturedati.alberi;

import java.util.Iterator;

public interface Albero<T> extends Iterable<T> {
	/*
		@return valore corrente dell'albero
	*/
	public T val();
	/*
		@return riferimento al padre del nodo
				corrente
	*/	
	public Albero<T> padre();
	/*
		@param pos posizione per ottenere un figlio
		@return riferimento dell'albero figlio in
				posizione pos
	*/
	public Albero<T> getFiglio(int pos);
	/*
		@return grado del corrente nodo
	*/
	public int grado();
	/*
		@retutn grado massimo del nodo
				corrente
	*/
	public int gradoMax();
	/*
		@return Iteratore sui figli
	*/
	public Iterator<Albero<T>> iteratorFigli();
} 
