# Alberi

Un **albero** (radicato) è una coppia _T = (N, A)_ costituita da un insieme _N_ di nodi e da un insieme _A_ al più strettamente contenuto in _N x N_ di coppie di nodi, dette **archi**. In un albero, ogni nodo _v_ (tranne la **radice**) ha un solo **genitore** (o **padre**) _u_ tale che ( _u, v_ ) appartenga ad _A_. 
Un nodo _u_ può avere zero o più **figli** _v_ tali che ( _u, v_ ) appartenga ad _A_, e il loro numero viene chiamato **grado** del nodo. Un nodo senza figli è chiamato **foglia**, mentre nodi che non sono né foglie né radice sono chiamati **nodi interni**. Gli **antenati** e **discendenti** di un nodo sono i nodi raggiungibili raggiungibile da un nodo salendo di padre in padre o scendendo di figlio in figlio, rispettivamente.
La **profondità** o **livello** di un nodo è il numero di archi che bisogna attraversare per raggiungerlo a partire dalla radice. Può essere definita ricorsivamente come segue:

* La radice ha profondità zero.
* Se un nodo ha profondità _k_, tutti i suoi figli hanno profondità _k + 1_.

Nodi con lo stesso genitore vengono detti **fratelli**, ed essi avranno quindi la stessa profondità. L' _altezza di un albero_ è la massima profondità a cui si trova una foglia

I principali vincoli strutturali riguardano i grado dei nodi e la profondità. In particolare, un albero _d_-ario è un albero in cui tutti i nodi tranne le foglie hanno grado _d_. Per _d = 2_ diremo che l'albero è **binario**. Infine diremo che un albero _d_-ario in cui tutte le foglie sono sullo stesso livello è **completo**.

## Rappresentazioni indicizzate
Gli alberi possono essere rappresentati utilizzando gli _array_ in due modi.

### Vettore padri
La più semplice rappresentazione di un albero tramite array è quella basata sul **vettore padri**.
Sia _T = (N, A)_ un albero con _n_ nodi numerati da 0 a ( _n_-1 ).

 Un vettore padri è un array _P_ di dimensione _n_ le cui celle contengono coppie ( _info, parent_ ):
* per ogni indice _v_ appartenente a [0, n-1], _P[v].info_ è il contenuto informativo del nodo _v_;
* _P[v].parent = u_ se e solo se vi è un arco ( _u, v_ ) appartenente ad _A_;
* se _v_ è la radice, allora _P[v].parent = null_.

### Vettore posizionale
Nel caso particolare degli alberi _d_-ari completi, con _d_ >= 2, è possibile usare una rappresentazione indicizzata dove ogni nodo ha una posizione prestabilita nella struttura. Sia  _T = (N, A)_ un albero _d_-ario completo con _n_ nodi numerati da 1 a _n_.

Un **vettore posizionale** è un array _P_ di dimensione _n_ tale che _P[v]_ contiene l'informazione associata al nodo _v_, e tale che l'informazione associata all _i_-esimo figlio di _v_ è in posizione _P[d v+1]_, per _i_ compreso tra 0 e ( _d_ - 1 ).

Per semplicità di indicizzazione, la posizione 0 dell'array non è utilizzata. Lo spazio richiesto per rappresentare un albero completo _d_-ario con _n_ nodi è quindi _n_+1. Dato un nodo _v_ il padre (se _v_ non è radice) è in posizione _[v/d]_.

## Rappresentazioni collegate
Le rappresentazioni collegate di alberi sono più flessibili di quelle indicizzate, e permettono di supportare modifiche alla struttura di un albero molto più efficientemente. L'idea di base è quella di rappresentare ogni nodo dell' albero con un record che contiene l'informazione associata al nodo, più altri puntatori che consentono di raggiungere altri nodi dell'albero.

### Puntatori ai figli
Se ogni nodo dell'albero ha grado al più _d_, è possibile mantenere in ogni nodo un puntatore a ciascuno dei possibili _d_ figli. Se un figlio è assente, si pone il puntatore a _null_.

### Lista figli
Se il numero massimo di figli non è noto a priori, è possibile associare ad ogni nodo una lista di puntatori ai suoi figli. Questa lista può essere a sua volta rappresentata in modo indicizzato o collegato.

### Primo figlio - fratello successivo
Come variante alla soluzione precedente senza dover usare una struttura dati addizionale (la lista figli) per ogni nodo, è possibile mantenere per ogni nodo un puntatore al primo figlio (posto a _null_ se non vi sono figli), e un puntatore al fratello successivo posto a _null_ se non vi sono fratelli successivi). Per scandire tutti i figli del nodo, basta quindi scendere sul primo figlio, e poi scorrere i fratelli successivi saltando di fratello in fratello.

## Alberi binari di ricerca
Un albero binario di ricerca è un albero binario che soddisfa le seguenti proprietà:

* ogni nodo _v_ contiene un elemento _elem(v)_ cui è associata una _chiave(v)_ presa da un dominio totalmente ordinato;
* le chiavi nel sottoalbero sinistro di _v_ sono <= _chiave(v)_
* le chiavi nel sottoalbero destro di _v_ sono >= _chiave(v)_.

Le chiavi possono essere di qualunque tipo, purché su di esse sia definita una relazione d'ordine totale. I punti 2 e 3 sono noti come **proprietà di ricerca**, e garantiscono che visitando un albero binario di ricerca in ordine simmetrico si ottengano le chiavi in ordine non decrescente.

### ricerca
Confrontiamo la chiave _x_ che stiamo cercando con la chiave _chiave(v)_ della radice _v_ dell' albero di ricerca. Se sono uguali, abbiamo trovato l' elemento. Altrimenti, proseguiamo la ricerca ne sottoalbero sinistro se _x < chiave(v)_ o in quello destro se _x > chiave(v)_. Ripetiamo la ricerca a partire dal figlio sinistro o destro della radice usando questa stessa strategia, finché non troviamo _x_ oppure arriviamo ad un albero vuoto, nel qual caso _x_ non appartiene all'albero.

### inserimento
Un nuovo nodo con elemento _e_ e chiave _k_ viene sempre inserito come foglia dell'albero di ricerca.

* Cerca il nodo _v_ che diventerà genitore del nuovo nodo.
* Crea un nuovo nodo _u_ con elemento _e_ e chiave _k_ed appendilo come figlio sinistro o destro di _v_ rispettando la proprietà di ricerca.

### massimo
Con la proprietà di ricerca, per trovare il massimo di un sottoalbero radicato in qualunque nodo _u_ basta scendere verso destra nell'albero finché possibile.

### predecessore
Il predecessore di un nodo _u_ è un nodo _v_ avente massima chiave _<= chiave(u)_. Per trovare il predecessore di _u_ distinguiamo due casi:

* _u_ ha un figlio sinistro: in tal caso _pred(u)_ è il massimo del sottoalbero sinistro di _u_;
* _u_ non ha un figlio sinistro: _pred(u)_, se esiste, è il più basso antenato di _u_ (ovvero, l'antenato di _u_ con massima profondità nell'albero) il cui figlio destro è anche esso antenato di _u_. Per trovarlo risaliamo da _u_ verso la radice fino ad incontrare la prima "svolta a sinistra".

### rimozione
L'operazione di rimozione è più difficoltosa dell'operazione di inserimento. Per implementare l'operazione di rimozione useremo la funzione predecessore per la ricerca del predecessore di un nodo. Sia _u_ il nodo contenente l'elemento _e_ da cancellare. Si distinguono tre casi:

* _u_ è una foglia: in questo caso basta distaccare la foglia dal genitore ed eliminarla;
* _u_ ha un unico figlio; sia _v_ l'unico figlio di _u_. Se _u_ è radice, _v_ diviene la nuova radice dell'albero. Altrimenti, dopo aver individuato il genitore _w_ di _u_, l'arco _(w, u)_ viene sostituito dall'arco _(w, v)_;
* _u_ ha due figli: ci si riconduce ad uno dei casi precedenti operando come segue. Si individua il predecessore di _u_, diciamo _v_: tale predecessore è certamente il massimo del sottoalbero sinistro di _u_, poiché _u_ ha due figli. Osserviamo che _v_ non può avere due figli, altrimenti non sarebbe predecessore. Dopo aver copiato _chiave(v)_ in _chiave(u)_, è quindi possibile cancellare fisicamente _v_ dall'albero applicando uno dei due casi precedenti.


