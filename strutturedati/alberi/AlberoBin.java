package strutturedati.alberi;

public interface AlberoBin<T> extends Albero<T> {
	public AlberoBin<T> figlioSinistro();
	public AlberoBin<T> figlioDestro();
}
